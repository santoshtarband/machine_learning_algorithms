import pandas as pd
import numpy as np
import sklearn
import matplotlib.pyplot as plt
import pickle
from sklearn import linear_model
from matplotlib import style

# read data set from csv file using pandas
data = pd.read_csv("./dataset/student-mat.csv", sep=";")

# collect attriburtes
data = data[["G1", "G2", "G3", "studytime", "failures", "absences"]]

# label to predict
predict = "G3"

# construct X array which has all attributes
X = np.array(data.drop([predict], 1))

# construct y array which is the prediction
y = np.array(data[predict])

# split test train data
x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.10)

# best = 0
# for _ in range(30):
#     # split test train data
#     x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.10)
#
#     # choose Linear Regression model
#     linear = linear_model.LinearRegression()
#
#     # fix the training data
#     linear.fit(x_train, y_train)
#
#     # read accuracy of the fit
#     accuracy = linear.score(x_train, y_train)
#     print(accuracy)
#     if accuracy > best:
#         best = accuracy
#         with open("student_grade.pkl", "wb") as file:
#             pickle.dump(linear, file)

pickle_in = open("student_grade.pkl", "rb")
linear = pickle.load(pickle_in)

# show coefficients and intercept of the model
print(f"Co: {linear.coef_}")
print(f"Intercept: {linear.intercept_}")

# new predictions(y) using test data
predictions = linear.predict(x_test)

# predictions vs y_test
for x in range(len(predictions)):
    print(predictions[x], y_test[x], x_test[x])

style.use("ggplot")

# Data exploration
# p = "failures"
# style.use("ggplot")
# plt.scatter(data[p], data["G3"])
# plt.xlabel(p)
# plt.ylabel("Final Grade")
# plt.show()

# predictions vs test data
plt.scatter(predictions, y_test)
plt.xlabel("Predictions")
plt.ylabel("Test data")
plt.show()
