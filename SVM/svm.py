import sklearn
from sklearn import datasets
from sklearn import svm
from sklearn import metrics
from sklearn.neighbors import KNeighborsClassifier

cancer = datasets.load_breast_cancer()
# print(cancer.feature_names)
# print(cancer.target_names)

X = cancer.data
y = cancer.target

x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.2)

# print(x_train)
# print(y_train)

# Classifying with SVM
clf = svm.SVC(kernel="linear", C=2)
# clf = svm.SVC(kernel="poly", degree=2)

clf.fit(x_train, y_train)

y_prediction = clf.predict(x_test)

acc_svm = metrics.accuracy_score(y_test, y_prediction)

print(f"SVM Accuracy: {acc_svm * 100:.2f}")

# Retrying with KNN
clf = KNeighborsClassifier(n_neighbors=13)

clf.fit(x_train, y_train)

y_prediction = clf.predict(x_test)

acc_knn = metrics.accuracy_score(y_test, y_prediction)

print(f"KNN Accuracy: {acc_knn * 100:.2f}")
