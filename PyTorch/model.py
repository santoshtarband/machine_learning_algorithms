import torch
import torch.nn as nn
import torch.nn.functional as funcs
import torch.optim as optim
import matplotlib.pyplot as plt

from torchvision import transforms, datasets

# Download the MNIST digits datset and split test and train data
train = datasets.MNIST("", train=True, download=True,
                       transform=transforms.Compose([transforms.ToTensor()]))
test = datasets.MNIST("", train=False, download=True,
                      transform=transforms.Compose([transforms.ToTensor()]))

# Create test and train datasets
trainset = torch.utils.data.DataLoader(train, batch_size=10, shuffle=True)
testset = torch.utils.data.DataLoader(test, batch_size=10, shuffle=True)

# Defining/Designing the Neural Network
class Net(nn.Module):
    def __init__(self):
        super().__init__()
        self.fc1 = nn.Linear(28 * 28, 64)  # input = 28*28 and output = 64
        self.fc2 = nn.Linear(64, 64)
        self.fc3 = nn.Linear(64, 64)
        self.fc4 = nn.Linear(64, 10)  # output = 10 {ten digits}

    def forward(self, x):
        x = funcs.relu(self.fc1(x))
        x = funcs.relu(self.fc2(x))
        x = funcs.relu(self.fc3(x))
        x = self.fc4(x)
        return funcs.log_softmax(x, dim=1)


# Initialize the network
net = Net()

# Define the optimizer function
optimizer = optim.Adam(net.parameters(), lr=0.001)
EPOCHS = 3

# Minimize the loss function
for epoch in range(EPOCHS):
    for data in trainset:
        X, y = data
        net.zero_grad()
        output = net(X.view(-1, 28 * 28))
        loss = funcs.nll_loss(output, y)
        loss.backward()
        optimizer.step()
    print(loss)

# Measuring the accuracy of the model with test data
correct = 0
total = 0
with torch.no_grad():
    for data in testset:
        X, y = data
        output = net(X.view(-1, 784))
        for idx, i in enumerate(output):
            if torch.argmax(i) == y[idx]:
                correct += 1
            total += 1
print(f"Accuracy: {round((correct / total) * 100, 3)}%")

# Using the model to make prediction
plt.imshow(X[0].view(28, 28))
plt.title(f"{torch.argmax(net(X[0].view(-1, 784))[0])}")
plt.show()
