import pandas as pd
import numpy as np
import sklearn
from sklearn.utils import shuffle
from sklearn.neighbors import KNeighborsClassifier
from sklearn import linear_model, preprocessing

cols = ["buying", "maint", "door", "persons", "lug_boot", "safety", "class"]
data = pd.read_csv("./dataset/car.data", names=cols)
# print(data.head())

le = preprocessing.LabelEncoder()
buying = le.fit_transform(list(data["buying"]))
maint = le.fit_transform(list(data["maint"]))
door = le.fit_transform(list(data["door"]))
persons = le.fit_transform(list(data["persons"]))
lug_boot = le.fit_transform(list(data["lug_boot"]))
safety = le.fit_transform(list(data["safety"]))
cls = le.fit_transform(list(data["class"]))

predict = "class"
X = list(zip(buying, maint, door, persons, lug_boot, safety))
y = list(cls)

x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(X, y, test_size=0.1)

model = KNeighborsClassifier(n_neighbors=5)
model.fit(x_train, y_train)
accuracy = model.score(x_test, y_test)
print(f"Accuracy: {accuracy*100:.2f} %")

predicted = model.predict(x_test)

names = ["unacc", "acc", "good", "vgood"]

for x in range(len(x_test)):
    print(f"Predicted: {names[predicted[x]]}, Data: {x_test[x]}, Acutal Data: {names[y_test[x]]}")
    n = model.kneighbors([x_test[x]], 5, True)
    print(f"N: {n}")
